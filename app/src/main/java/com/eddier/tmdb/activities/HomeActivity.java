package com.eddier.tmdb.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.eddier.tmdb.R;
import com.eddier.tmdb.adapter.MovieAdapter;
import com.eddier.tmdb.model.MovieModel;
import com.eddier.tmdb.utils.PropertiesData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    List<MovieModel> listMoview;
    PropertiesData propertiesData;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);
        propertiesData= new PropertiesData(this);
        getData();
    }

    private void getData(){
        RequestQueue queue= Volley.newRequestQueue(this);
        String url=propertiesData.getUrl_api().concat(propertiesData.getApi_key());
        JsonObjectRequest objectRequest= new JsonObjectRequest(Request.Method.GET, url, null ,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject r=response;
                try {
                    listMoview = new Gson().fromJson(r.get(propertiesData.getReponse_result()).toString(),
                            new TypeToken<List<MovieModel>>(){}.getType());
                    RecyclerView recyclerView= findViewById(R.id.list_movies_rcv);
                    MovieAdapter movieAdapter= new MovieAdapter(HomeActivity.this,listMoview, propertiesData);
                    recyclerView.setLayoutManager(new GridLayoutManager(HomeActivity.this,3));
                    recyclerView.setAdapter(movieAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(objectRequest);
    }

}