package com.eddier.tmdb.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.eddier.tmdb.R;
import com.eddier.tmdb.utils.PropertiesData;
import com.squareup.picasso.Picasso;

public class DescriptionMovieActivity extends AppCompatActivity {
    ImageView imgMovie;
    TextView overView, titleMovie,releaseDate, adult, popularity;
    PropertiesData propertiesData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        propertiesData= new PropertiesData(this);
        setContentView(R.layout.activity_description_movie);
        imgMovie= findViewById(R.id.description_movie_img_view);
        overView= findViewById(R.id.over_view);
        titleMovie= findViewById(R.id.title_movie);
        releaseDate= findViewById(R.id.release_date);
        Intent intent= getIntent();
        String url =intent.getExtras().getString(propertiesData.getUrlImageMovieRequest());
        Picasso.get().load(url).into(imgMovie);
        String titleMovieRquest =intent.getExtras().getString(propertiesData.getTitleMovieRquest());
        titleMovie.setText("Title: ".concat(titleMovieRquest));
        String releaseDateRequest=intent.getExtras().getString(propertiesData.getReleaseDateRequest());
        releaseDate.setText("Release Date: ".concat(releaseDateRequest));
        Double popularityRequest=intent.getExtras().getDouble(propertiesData.getPopularityRequest());
        popularity=findViewById(R.id.popularity);
        popularity.setText("Popularity: ".concat(popularityRequest.toString()));
        boolean adultRequest=intent.getExtras().getBoolean(propertiesData.getAdultRequest());
        adult= findViewById(R.id.adult);
        adult.setText("Adult: ".concat(adultRequest==true?"Yes":"No"));
        String overViewRequest= intent.getExtras().getString(propertiesData.getOverViewRequest());
        overView.setText(overViewRequest);

    }
}