package com.eddier.tmdb.utils;

import android.content.Context;

import com.eddier.tmdb.R;

public class PropertiesData {

    String url_api;
    String api_key;
    String  reponse_result;
    String titleMovieRquest;
    String urlImageMovieRequest;
    String overViewRequest;
    String releaseDateRequest;
    String popularityRequest;
    String adultRequest;
    String profijoUrlImageMovie;
    public PropertiesData(Context context){
        this.url_api = context.getString(R.string.url_api);
        this.api_key = context.getString(R.string.api_key);
        this.reponse_result =context.getString(R.string.response_results);
        this.titleMovieRquest=context.getString(R.string.titleMovieRquest);
        this.urlImageMovieRequest=context.getString(R.string.urlImageMovieRequest);
        this.overViewRequest=context.getString(R.string.overViewRequest);
        this.releaseDateRequest=context.getString(R.string.releaseDateRequest);
        this.popularityRequest= context.getString(R.string.popularityRequest);
        this.adultRequest=context.getString(R.string.adultRequest);
        this.profijoUrlImageMovie=context.getString(R.string.profijoUrlImageMovie);
    }

    public String getUrl_api() {
        return url_api;
    }

    public String getApi_key() {
        return api_key;
    }

    public String getReponse_result() {
        return reponse_result;
    }

    public String getTitleMovieRquest() {
        return titleMovieRquest;
    }

    public String getUrlImageMovieRequest() {
        return urlImageMovieRequest;
    }

    public String getOverViewRequest() {
        return overViewRequest;
    }

    public String getReleaseDateRequest() {
        return releaseDateRequest;
    }

    public String getPopularityRequest() {
        return popularityRequest;
    }

    public String getAdultRequest() {
        return adultRequest;
    }

    public String getProfijoUrlImageMovie() {
        return profijoUrlImageMovie;
    }
}
