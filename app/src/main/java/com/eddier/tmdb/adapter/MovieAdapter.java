package com.eddier.tmdb.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eddier.tmdb.activities.DescriptionMovieActivity;
import com.eddier.tmdb.R;
import com.eddier.tmdb.model.MovieModel;
import com.eddier.tmdb.utils.PropertiesData;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyVewHolder> {

    private Context context;
    private List<MovieModel> listMovie;
    PropertiesData propertiesData;
    public MovieAdapter(Context context, List<MovieModel> listMovie, PropertiesData propertiesData) {
        this.context = context;
        this.listMovie = listMovie;
        this.propertiesData=propertiesData;
    }

    @NonNull
    @Override
    public MyVewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater lInflater=LayoutInflater.from(context);
        view=lInflater.inflate(R.layout.movie_cardview,parent,false);
        return new MyVewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVewHolder holder, int position) {
        String url =propertiesData.getProfijoUrlImageMovie().concat(listMovie.get(position).getPoster_path());
        Picasso.get().load(url).into(holder.moview_img);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, DescriptionMovieActivity.class);
                intent.putExtra(propertiesData.getUrlImageMovieRequest(),url);
                intent.putExtra(propertiesData.getOverViewRequest(),listMovie.get(position).getOverview());
                intent.putExtra(propertiesData.getTitleMovieRquest(),listMovie.get(position).getTitle());
                intent.putExtra(propertiesData.getReleaseDateRequest(), listMovie.get(position).getRelease_date());
                intent.putExtra(propertiesData.getPopularityRequest(), listMovie.get(position).getPopularity());
                intent.putExtra(propertiesData.getAdultRequest(), listMovie.get(position).isAdult());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public static class MyVewHolder extends RecyclerView.ViewHolder{
        ImageView moview_img;
        CardView cardView;
        public MyVewHolder(@NonNull View itemView) {
            super(itemView);
            moview_img= itemView.findViewById(R.id.movie_img_view);
            cardView=itemView.findViewById(R.id.card_view_pelicula);
        }
    }
}
